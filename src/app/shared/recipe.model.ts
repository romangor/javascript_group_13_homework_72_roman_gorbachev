export class Recipe {
  constructor(
    public title: string,
    public imgUrlRecipe: string,
    public description: string,
    public ingredients: string,
    public steps: [{
      imgStepUrl: string;
      stepDescription: string;
    }],
    public id: string,
  ) {}
}
