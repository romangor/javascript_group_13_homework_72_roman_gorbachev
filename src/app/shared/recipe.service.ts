import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';


@Injectable()
export class RecipeService {
  recipesChange = new Subject<Recipe[]>();
  sendingRecipe = new Subject<boolean>();
  removingRecipe = new Subject<boolean>();
  fetchingRecipes = new Subject<boolean>();

  recipes!: Recipe[];
  constructor(private http: HttpClient,
              private router: Router) {}

  sendRecipe(recipe: Recipe){
    this.sendingRecipe.next(true);
    const body = {
      title: recipe.title,
      imgUrlRecipe: recipe.imgUrlRecipe,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    }
    this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/recipes.json', body).subscribe(() =>{
    this.fetchRecipes();
    this.sendingRecipe.next(false);
    void this.router.navigate(['recipes']);
    });
  }

  editRecipe(recipe: Recipe){
    this.sendingRecipe.next(true);
    const body = {
      title: recipe.title,
      imgUrlRecipe: recipe.imgUrlRecipe,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    }
    this.http.put(`https://projectsattractor-default-rtdb.firebaseio.com/recipes/${recipe.id}.json`, body).subscribe(() =>{
      this.fetchRecipes();
      this.sendingRecipe.next(false);
      void this.router.navigate(['recipes']);
    });
  }

  fetchRecipes(){
    this.fetchingRecipes.next(true);
    this.http.get<{[id: string]: Recipe}>('https://projectsattractor-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        if (result === null){
          return [];
        }
        return Object.keys(result).map(id => {
          const dataRecipe = result[id];
          return new Recipe(
            dataRecipe.title,
            dataRecipe.imgUrlRecipe,
            dataRecipe.description,
            dataRecipe.ingredients,
            dataRecipe.steps,
            id);
        });
      }))
      .subscribe( recipes => {
        this.recipes = recipes;
        this.recipesChange.next(this.recipes.slice());
        this.fetchingRecipes.next(false);
      })
  }

  fetchRecipe(id: string){
    return this.http.get<Recipe| null>(`https://projectsattractor-default-rtdb.firebaseio.com/recipes/${id}.json`)
      .pipe(map(result => {
        if(!result) return null;
        return  new Recipe(
          result.title,
          result.imgUrlRecipe,
          result.description,
          result.ingredients,
          result.steps,
          id);
      }));
  }

  removeRecipe(id: string){
    this.removingRecipe.next(true)
    this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com/recipes/${id}.json`).subscribe(() => {
      this.fetchRecipes();
      this.removingRecipe.next(false);
    });
  }
}
