import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from 'src/app/shared/recipe.model';
import { RecipeService } from '../../shared/recipe.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit, OnDestroy {
  @Input() recipe!: Recipe;

  removing!: boolean;
  removingSubscriptions!: Subscription;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.removingSubscriptions = this.recipeService.removingRecipe.subscribe((isRemoved: boolean) => {
      this.removing = isRemoved;
    });
  }

  onDelete(id: string) {
    this.recipeService.removeRecipe(id);
  }


  ngOnDestroy() {
    this.removingSubscriptions.unsubscribe();
  }
}
