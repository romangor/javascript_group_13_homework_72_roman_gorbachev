import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Recipe } from 'src/app/shared/recipe.model';
import { RecipeService } from 'src/app/shared/recipe.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-recipes',
  templateUrl: './edit-recipes.component.html',
  styleUrls: ['./edit-recipes.component.css']
})
export class EditRecipesComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  sending!: boolean;
  isEdit!: boolean;
  editId!: string;

  sendingSubscription!: Subscription;
  constructor(private recipeService: RecipeService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      const recipe = <Recipe | null>data.recipe;
      if (recipe){
          this.form = new FormGroup( {
            title: new FormControl(recipe.title, Validators.required),
            imgUrlRecipe: new FormControl(recipe.imgUrlRecipe, Validators.required),
            description: new FormControl(recipe.description, Validators.required),
            ingredients: new FormControl(recipe.ingredients, Validators.required),
            steps: new FormArray([]),
          });
        this.isEdit = true;
        this.editId = recipe.id;
        this.setStep(recipe.steps);
      } else {
        this.form = new FormGroup( {
            title: new FormControl('', Validators.required),
            imgUrlRecipe: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
            ingredients: new FormControl('', Validators.required),
            steps: new FormArray([]),
          });
      }
    });
    this.sendingSubscription = this.recipeService.sendingRecipe.subscribe( (isSending: boolean) => {
      this.sending = isSending;
    });
  }

  fieldHasError(fieldName: string, errorType: string){
    const field = this.form.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  getStepsControls() {
      const steps = <FormArray>(this.form.get('steps'));
      return steps.controls;
    }

    setStep(stepsFromRoute: [{ imgStepUrl:string, stepDescription: string;}]){
      const steps = <FormArray>this.form.get('steps');
      stepsFromRoute.forEach( step => {
         const stepsGroup = new FormGroup({
          imgStepUrl: new FormControl(step.imgStepUrl, Validators.required),
          stepDescription: new FormControl(step.stepDescription, Validators.required),
        });
        steps.push(stepsGroup);
      });
    }

  addStep(){
    const steps = <FormArray>this.form.get('steps');
    const stepsGroup = new FormGroup({
      imgStepUrl: new FormControl('', Validators.required),
      stepDescription: new FormControl('', Validators.required),
    });
    steps.push(stepsGroup);
  }

  addRecipe() {
    const id = '';
    const recipe = new Recipe(
      this.form.value.title,
      this.form.value.imgUrlRecipe,
      this.form.value.description,
      this.form.value.ingredients,
      this.form.value.steps,
      id || this.editId);
    if(this.isEdit){
      this.recipeService.editRecipe(recipe);
    } else {
      this.recipeService.sendRecipe(recipe);
    }
  }

  removeStep(index: number) {
    const steps = <FormArray>this.form.get('steps');
    steps.removeAt(index);
  }

  ngOnDestroy() {
    this.sendingSubscription.unsubscribe();
  }

}
