import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../shared/recipe.model';
import { RecipeService } from '../shared/recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit, OnDestroy {
  recipes!: Recipe[];
  loading!: boolean;

  loadingSubscription!: Subscription;
  recipesSubscriptions!: Subscription;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.recipeService.fetchRecipes();
    this.recipesSubscriptions = this.recipeService.recipesChange.subscribe(recipes => {
      this.recipes = recipes;
    });
    this.loadingSubscription = this.recipeService.fetchingRecipes.subscribe( (isFetching: boolean) => {
      this.loading = isFetching;
      console.log(isFetching)
    });
  }

  ngOnDestroy() {
    this.recipesSubscriptions.unsubscribe();
    this.loadingSubscription.unsubscribe();
  }
}
