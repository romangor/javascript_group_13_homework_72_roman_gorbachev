import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { RecipeService } from './recipe.service';
import { EMPTY, Observable, of } from 'rxjs';
import { Recipe } from './recipe.model';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService {

  constructor(private recipeService: RecipeService,
              private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Observable<never>{
    const recipeId = route.params['id'];
    return this.recipeService.fetchRecipe(recipeId).pipe(mergeMap(recipe => {
      if(recipe) {
        return of(recipe);
      }
      void this.router.navigate(['/recipes']);
      return EMPTY;
    }))
  }
}
