import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RecipesComponent } from './recipes/recipes.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { EditRecipesComponent } from './recipes/edit-recipes/edit-recipes.component';
import { DetailsRecipeComponent } from './recipes/details-recipe/details-recipe.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RecipeService } from './shared/recipe.service';
import { AppRoutingModule } from './app-routing.module';
import { RecipeItemComponent } from './recipes/recipe-item/recipe-item.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipesComponent,
    ToolbarComponent,
    EditRecipesComponent,
    DetailsRecipeComponent,
    RecipeItemComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
