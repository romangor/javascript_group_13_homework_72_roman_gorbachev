import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsRecipeComponent } from './recipes/details-recipe/details-recipe.component';
import { EditRecipesComponent } from './recipes/edit-recipes/edit-recipes.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeResolverService } from './shared/recipe-resolver.service';


const routes: Routes = [
  {path: 'recipes', component: RecipesComponent},
  {path: 'recipes/:id', component: DetailsRecipeComponent,
    resolve: {
      recipe: RecipeResolverService
    }},
  {path: 'recipes/:id/edit', component: EditRecipesComponent,
   resolve: {
    recipe: RecipeResolverService
   }},
  {path: 'edit', component: EditRecipesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
